extends Control

onready var control: Control = $Control
onready var content: Label = $Control/Content

func _ready():
	remove_child(control)
	control.visible = true

func _on_note_found(text: String):
	content.text = text
	add_child(control)


func disable():
	remove_child(control)
