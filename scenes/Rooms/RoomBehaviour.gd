extends Node

var bg_tilemap : TileMap
var mouse_pos : Vector2
var door_id

func _ready():
	bg_tilemap = get_parent().get_node('BG_TileMap')
	# may be done in a cleaner way I guess
	door_id = bg_tilemap.tile_set.find_tile_by_name("wall_door")

func process_click():
	# get tile id in the background tilemap :
	# get local position of mouse in room and the position in the tilemap
	var tile_position = bg_tilemap.world_to_map(mouse_pos - (get_parent() as Node2D).position)
	# then get the id of the tile at this location
	var tile_id = bg_tilemap.get_cellv(tile_position)
	# it's a door !
	if (tile_id == door_id):
		get_parent().transition_to_room(tile_position)

func get_room_size() -> Vector2:
	return bg_tilemap.get_used_rect().size * bg_tilemap.cell_size

func _input(event):
	# if mouse clic, check if it's a simple click and not a drag
	if (event is InputEventMouseButton):
		if(event.is_pressed()):
			mouse_pos = get_parent().get_global_mouse_position()
		elif (mouse_pos == get_parent().get_global_mouse_position()):
			process_click()
