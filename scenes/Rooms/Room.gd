extends Node2D

# links to Rooms based on door position in BG_TileMap
export var room_links : Dictionary
export (NodePath) var camera_path

# RoomBehaviour script, which handles the input
var room_behaviour
var camera

func _ready():
	room_behaviour = find_node("RoomBehaviour")
	# by default, the scene is not the one focused
	room_behaviour.set_process_input(false)
	camera = get_node(camera_path)
	if (camera):
		set_focus_room(true)

func set_focus_room(enable : bool):
	room_behaviour.set_process_input(enable)
	# move camera to position and change zoom
	if (enable):
		camera.set_target(position, room_behaviour.get_room_size())

func transition_to_room(door_position : Vector2):
	if (!room_links.has(door_position)):
		printerr("No room specified from door position ", door_position, " on room ", self.name)
		return
	var next_room = get_node(room_links[door_position])
	set_focus_room(false)
	next_room.camera = camera
	camera = null
	next_room.set_focus_room(true)
