extends Camera2D

var target_position : Vector2
var target_zoom : Vector2
var is_moving : bool
var current_room_size : Vector2

# speed of the transition
export var speed = 1.0
export var max_zoom_step = 0.01
export var zoom_margin := Vector2(0.05,0.05)

func _ready():
	get_viewport().connect("size_changed", self, "_on_viewport_size_change")

func _on_viewport_size_change():
	set_target_zoom(current_room_size) # recalculate zoom with new viewport
	is_moving = true

func _process(delta):
	if (is_moving):
		if (position == target_position && zoom == target_zoom):
			is_moving = false
		update_position(delta)
		update_zoom(delta)

func update_position(delta):
	var delta_move = target_position - position
	delta_move = delta_move.clamped(speed*delta)
	translate(delta_move)

func update_zoom(delta):
	var delta_zoom = target_zoom - zoom
	if (abs(delta_zoom.x) > max_zoom_step*delta):
		delta_zoom = sign(delta_zoom.x) * delta * Vector2(max_zoom_step,max_zoom_step)
	zoom += delta_zoom

func set_target_zoom(room_size):
	var viewport_size = get_viewport().size
	var new_scale = max(room_size.x / viewport_size.x, room_size.y / viewport_size.y)
	target_zoom = Vector2(new_scale, new_scale) + zoom_margin

func set_target(room_corner, room_size):
	target_position = room_corner + 0.5*room_size
	current_room_size = room_size # we store it in case of window resize
	set_target_zoom(room_size)
	is_moving = true
