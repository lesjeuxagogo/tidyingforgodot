extends Area2D

export (String) var text

signal note_found(text)

func _input_event(viewport, event, shape_idx):
	var click_event: InputEventMouseButton = event as InputEventMouseButton
	if click_event and not click_event.pressed:
		emit_signal("note_found", text)
